"use strict"

//Форма

let form = document.querySelector('.characteristics__form');
let button = document.querySelector('button.characteristics__submit-button');
let inputName = form.name;
let mistakeName = form.getElementsByClassName('characterictics__input-text-mistake')[0];
let inputGrade = form.rating;
let mistakeGrade = form.getElementsByClassName('characterictics__input-grade-mistake')[0];

form.addEventListener(`submit`, (e) => {
    e.preventDefault();
    handleSubmit(); 
});

function handleSubmit() {
    let nameUs = document.querySelector(`.characterictics__input-text`).value;
    let rating = +document.querySelector(`.characterictics__input-grade`).value;

    if (nameUs === ``) {
        mistakeGrade.style.visibility = `hidden`;
        mistakeName.style.visibility = `visible`;
    }
    else if (nameUs !== `` && nameUs.length < 2) {
        mistakeGrade.style.visibility = `hidden`;
        mistakeName.innerHTML = `Имя не может быть короче 2-х символов`;
        mistakeName.style.visibility = `visible`;
    }
    else if (nameUs !== `` && nameUs.length >= 2) {
        mistakeName.style.visibility = `hidden`;
        mistakeGrade.style.visibility = `visible`;
    }
    if (rating > 0 && rating < 6) {
        mistakeGrade.style.visibility = `hidden`;
    }
    if (nameUs !== `` && nameUs.length >= 2 && rating > 0 && rating < 6) {
        form.reset();
        localStorage.clear(formData);
    }
}

function hideMistakeName() {
    mistakeName.style.visibility = `hidden`;
};


function hideMistakeGrade() {
    mistakeGrade.style.visibility = `hidden`
};


let formData = {};
form.addEventListener(`input`, function (e) {
    formData[e.target.name] = e.target.value;
    localStorage.setItem(`formData`, JSON.stringify(formData));
});

if (localStorage.getItem(`formData`)) {
    formData = JSON.parse(localStorage.getItem(`formData`));
    for (let key in formData) {
        form.elements[key].value = formData[key];
        console.log(formData[key])
    }
}

// Корзина

let basket = document.querySelector(`.logo__basket-number`);
let basketBtn = document.querySelector(`.side-bar__add-button`);
basketBtn.value = +localStorage.getItem(`countBasket`)
basketBtn.onclick = getBasket;
function getBasket() {
    if (+basketBtn.value === 0) {
        basketBtn.value += 1;
        console.log(+basketBtn.value);
        basketBtn.style.background = `#888888`;
        basketBtn.innerHTML = `Товар уже в корзине`;
        localStorage.setItem(`countBasket`, +basketBtn.value);
        basket.style.visibility = `visible`;
    }
    else {
        basketBtn.value -= 1;
        console.log(+basketBtn.value);
        basketBtn.style.background = ``;
        basketBtn.innerHTML = `Добавить в корзину`;
        localStorage.removeItem(`countBasket`, +basketBtn.value);
        basket.style.visibility = `hidden`;
    }
};
if (+basketBtn.value === 1) {
    basketBtn.style.background = `#888888`;
    basketBtn.innerHTML = `Товар уже в корзине`;
    basket.style.visibility = `visible`;
} else {
    basketBtn.style.background = ``;
    basketBtn.innerHTML = `Добавить в корзину`;
    basket.style.visibility = `hidden`;
};
