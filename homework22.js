"use strict";

// Упражнение 1
function getSumm(arr) {
    let sum = 0;
    arr.forEach(function (n) {
        if (typeof n === "number") {
            sum += n;
        }
    })
    return sum;
}

let arr1 = [1, 2, 10, 5];
alert (getSumm(arr1));  // 18
let arr2 = ["a", {}, 3, 3, -2];
alert(getSumm(arr2));    // 4

// Упражнение 3
function addToCart(id) {
    let hasInCart = cart.includes(id);
    if (hasInCart) return;
    let set = new Set(cart);
    set.add(id);
    return (cart = Array.from(set));
}

function removeFromCart(id) {
    cart.splice(cart.indexOf(id), 1);
    return cart;
}


// В корзине один товар
let cart = [4884];

// Добавили товар
addToCart(3456);
console.log(cart); // [4884, 3456]

// Повторно добавили товар
addToCart(3456);
console.log(cart);// [4884, 3456]

// Удалили товар
removeFromCart(4884);
console.log(cart);// [3456]


