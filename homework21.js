"use strict";
// Упражнение 1
function isEmpty(obj) {
  for (let key in obj) {
    return false;
  }
  return true;
}
/**
 * Checks if object has any properties.
 * @return {boolean} if object has at least one property.
 */


// Упражнение 3
let salaries = {
  John: 100000,
  Ann: 160000,
  Pete: 130000,
};

function raiseSalary(perzent) {
  let newSalaries = {};
  for (let key in salaries) {
    let raise = (salaries[key] * perzent) / 100;
    newSalaries[key] = salaries[key] + raise;
  }
  return newSalaries;
}

let result = raiseSalary(5);

function roundSalary(obj) {
  let roundSalaries = {};
  for (let key in obj) {
    roundSalaries[key] = Math.floor(obj[key]);
  }
  console.log(roundSalaries);
  return roundSalaries;
}
let salary = roundSalary(result);

function calcSumm(obj) {
  let summ = 0;
  for (let key in obj) {
    summ += obj[key];
  }
  return summ;
}
console.log(calcSumm(salary));