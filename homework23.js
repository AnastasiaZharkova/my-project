"use strict";

// Упражнение 1

let n = 0;
let countTimer = prompt("Введите число для обратного отсчёта", 0);
let countTimerNumber = Number(countTimer);
if (isNaN(countTimerNumber)) {
    console.log("Вы ввели не число!");
} else {
    n = countTimerNumber;
    let timerId = setInterval(function () {
        if (n <= 0) {
            console.log("Время вышло!");

            clearTimeout(timerId);
        } else {
            console.log("Осталось " + n + " секунд");
        }

        n -= 1;

    }, 1000);
}

// Упражнение 2

let count = 0;
let intervalId2 = setInterval(() => {
    count = count + 1;
}, 1);
let promise2 = fetch("https://reqres.in/api/users");
promise2
    .then(response => response.json())
    .then(result => UserInfo(result))
    .catch(error => console.debug({ error }));

function UserInfo(obj) {
    console.log("Получили пользователей: " + obj.data.length);
    obj.data.forEach(element => {
        console.log("- " + element.first_name + " " + element.last_name + " (" + element.email + ")")
    });
    clearInterval(intervalId2);
    console.log("Время выполнения запроса (мс) = " + count);

};
