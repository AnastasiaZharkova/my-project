"use strict";
// Упражнение 1
for (let i = 0; i <= 20; i += 2) {
    console.log(i)
} 
// Упражнение 2
let sum = 0;
let count = 0;
while (count !== 3) {
    let value = +prompt("Введите число", "");
    if (!value) alert ("Ошибка, вы ввели не число");
    if (!value) break;
    sum += value;
    count++;
}
if (count === 3) {
    alert("Сумма: " + sum);
}
// Упражнение 3
function getNameOfMonth(n) {
    if (n === 0) return `Январь`;
    if (n === 1) return `Февраль`;
    if (n === 2) return `Март`;
    if (n === 3) return `Апрель`;
    if (n === 4) return `Май`;
    if (n === 5) return `Июнь`;
    if (n === 6) return `Июль`;
    if (n === 7) return `Август`;
    if (n === 8) return `Сентябрь`;
    if (n === 9) return `Октябрь`;
    if (n === 10) return `Ноябрь`;
    if (n === 11) return `Декабрь`;
}
for (let n = 0; n < 12; n++) {
    const month = getNameOfMonth(n);
    if (n === 9) continue;
    console.log(month);
}
  